$(function() {
  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
  };
  if(!isMobile.any()){
    $('footer').footerReveal();
  }

  function setupFixedSidebar() {
    var footerTop = $("footer").offset().top,
        scrollTop = $(window).scrollTop(),
        height = $(window).height();

    if(footerTop < scrollTop + height) {
      $('.right-content').css('top', footerTop - scrollTop - height);
    } else {
      $('.right-content').css('top', 0);
    }
  }

  $(window).scroll(setupFixedSidebar);
  $(window).resize(setupFixedSidebar);

});
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function phonenumber(inputtxt)
{
  var phoneno = /^\(([0-9]{2})\) ([0-9]{4})\ ([0-9]{4,5})$/;
  if(phoneno.test(inputtxt)){
    return true;
  }
  else{
    return false;
  }
}
function validateName(inputtxt){
  var nameReg = /^([a-zA-Z]{2,40} +[a-zA-Z]{2,40})$/;
  if(nameReg.test(inputtxt)){
    return true;
  }else{
    return false;
  }
}
$(function() {
  validation = {
    name : false,
    email : false,
    phone: false,
    nome: false,
  };
  $('.bottom-arrow-wrapper').click(function() {
      $('html, body').animate({scrollTop: $('.left-content').height()+140}, 300);
      return false;
  });
  $('#name').keyup(function(){
    if(!validateName($('#name').val())){
      validation.name = false;
      $('.name-tick').css("display","none");
    }else{
      validation.name = true;
      $('.name-tick').css("display","block");
    }
  });
  $('#email').keyup(function(){
    if(!isEmail($('#email').val())){
      validation.email = false;
      $('.email-tick').css("display","none");
    }
    else{
      validation.email = true;
      $('.email-tick').css("display","block");
    }
  });
  $('#phone').keyup(function(){
    if(!phonenumber($('#phone').val())){
      validation.phone = false;
      $('.phone-tick').css("display","none");
    }
    else{
      validation.phone = true;
      $('.phone-tick').css("display","block");
    }
  });
  $('#nome').keyup(function(){
    if($('#nome').val().length < 2){
      validation.nome = false;
      $('.nome-tick').css("display","none");
    }
    else if($('#name').val().length > 0){
      validation.nome = true;
      $('.nome-tick').css("display","block");
    }
  });
  $('.click-precisamos').click(function(){
    temp = true;
    if(validation.name == false){
      $('.name-pink').slideDown("fast");
      temp = false;
    }
    else{
      $('.name-pink').slideUp("fast");
      temp = true;
    }
    if(validation.email == false){
      $('.email-pink').slideDown("fast");
      temp = false;
    }
    else{
      $('.email-pink').slideUp("fast");
      temp = true;
    }
    if(validation.phone == false){
      $('.phone-pink').slideDown("fast");
      temp = false;
    }
    else{
      $('.phone-pink').slideUp("fast");
      temp = true;
    }
    if(validation.nome == false){
      $('.nome-pink').slideDown("fast");
      temp = false;
    }
    else{
      $('.nome-pink').slideUp("fast");
      temp = true;
    }
    alert(temp);
    if(temp==true){
      $('.desktop-wrapper').css('display','none');
      $('.submit-wrapper').css('display','block');
    }
  });
});
